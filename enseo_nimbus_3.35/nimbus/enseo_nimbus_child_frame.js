/**
 * @fileOverview HD2000 Nimbus Javascript API
 * <p><b>Copyright (c) Enseo, Inc. 2008
 *
 * This material contains trade secrets and proprietary information
 * and	may not	 be used, copied, disclosed or	distributed	 except
 * pursuant to	a license from Enseo, Inc. Use of copyright notice
 * does not imply publication or disclosure.</b>
 */

////////////////////////////////////////////////////////////////////////
//
// Reference   : $Source: /cvsroot/calica/apps/rootfs_common/root/usr/bin/data/html/js/Attic/enseo_nimbus_child_frame.js,v $
//
// Revision	   : $Revision: 1.2.92.1 $
//
// Date		   : $Date: 2015-12-09 09:56:22 $
//
// Author(s)   : Scott Horton
//
// Description : HD2000 Nimbus javascript API.  This file should be included
//               in place of enseo_nimbus.js when the HTML file is contained in
//				 an iframe.  The parent of the iframe must include the enseo_nimbus.js file.
//
////////////////////////////////////////////////////////////////////////

var	Nimbus = parent.Nimbus;
var NimbusFrameInstance = Nimbus? Nimbus.newFrameInstance(): null;

/**********************************************************************************************
***********************************************************************************************
**********************************************************************************************/

	
