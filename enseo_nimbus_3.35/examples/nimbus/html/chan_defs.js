//////////////////////////////////////////////////////////////////////////////////////////
//	Enseo Nimbus API usage example
//	Channel definitions
//	Copyright (c) Enseo, Inc. 1997-2006
//////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////
// Code for building the channel list used by the Nimbus example application
//////////////////////////////////////////////////////////////////////////////////////////

// Array of available channels
var ChanList = new Array();

// Build a channel list object
function BuildChannel(Label, ChanDesc) {
	var ChanObj = new Object();
	ChanObj.ChanDesc = ChanDesc;
	ChanObj.Label = Label;
	return ChanObj;
}

// Add channels to the list

ChanList.push(BuildChannel("Input, Local HDMI0", '<ChannelParams ChannelType="Input"> <InputChannelParams InputID="Local_DVI/HDMI0"> </InputChannelParams> </ChannelParams>'));

ChanList.push(BuildChannel("Input, Local HDMI1", '<ChannelParams ChannelType="Input"> <InputChannelParams InputID="Local_DVI/HDMI1"> </InputChannelParams> </ChannelParams>'));

ChanList.push(BuildChannel("Input, Local Component0", '<ChannelParams ChannelType="Input"> <InputChannelParams InputID="Local_Component0"> </InputChannelParams> </ChannelParams>'));

ChanList.push(BuildChannel("RF Analog, cable 78", '<ChannelParams ChannelType="Analog"> <AnalogChannelParams PhysicalChannelIDType="Cable" PhysicalChannelID="78"> </AnalogChannelParams> </ChannelParams>'));
Chan_RFAnalog1 = ChanList.length - 1;

ChanList.push(BuildChannel("RF digital (Air, down/up conv.), 8VSB, cable, 80.1", '<ChannelParams ChannelType="Digital"> <DigitalChannelParams PhysicalChannelIDType="Cable" PhysicalChannelID="80" DemodMode="8VSB" ProgramSelectionMode="PATProgram" ProgramID="1"> </DigitalChannelParams> </ChannelParams>'));

ChanList.push(BuildChannel("RF digital (TimeWarner), QAM256, cable, 107.1", '<ChannelParams ChannelType="Digital"> <DigitalChannelParams PhysicalChannelIDType="Cable" PhysicalChannelID="107" DemodMode="QAM256" ProgramSelectionMode="PATProgram" ProgramID="1"> </DigitalChannelParams> </ChannelParams>'));

ChanList.push(BuildChannel("RF digital (DekTec), QAM256, cable, 79.1", '<ChannelParams ChannelType="Digital"> <DigitalChannelParams PhysicalChannelIDType="Cable" PhysicalChannelID="79" DemodMode="QAM256" ProgramSelectionMode="PATProgram" ProgramID="1"> </DigitalChannelParams> </ChannelParams>'));
Chan_RFDigital1 = ChanList.length - 1;

ChanList.push(BuildChannel("RF digital (DekTec), QAMAuto, cable, 77.1",	'<ChannelParams ChannelType="Digital" AuthRequired="TRUE"> <DigitalChannelParams PhysicalChannelIDType="Cable" PhysicalChannelID="77" DemodMode="QAMAuto" ProgramSelectionMode="PATProgram"	ProgramID="1"> </DigitalChannelParams> </ChannelParams>'));

ChanList.push(BuildChannel("RF digital (LG-DirectTV), QAM256, cable, 81.1", '<ChannelParams ChannelType="Digital" AuthRequired="TRUE"> <DigitalChannelParams PhysicalChannelIDType="Cable" PhysicalChannelID="81" DemodMode="QAM256" ProgramSelectionMode="PATProgram" ProgramID="1"> </DigitalChannelParams> </ChannelParams>'));

ChanList.push(BuildChannel("RF digital (Floodgate), 14.1", '<ChannelParams ChannelType="Digital" AuthRequired="TRUE"> <DigitalChannelParams PhysicalChannelIDType="Cable" PhysicalChannelID="14" DemodMode="QAMAuto" ProgramSelectionMode="PATProgram" ProgramID="1"> </DigitalChannelParams> </ChannelParams>'));
Chan_IP1 = ChanList.length - 1;

ChanList.push(BuildChannel("IP, 239.255.224.23:1234", '<ChannelParams ChannelType="UDP"> <UDPChannelParams Address="239.255.224.23" Port="1234"> </UDPChannelParams> </ChannelParams>'));
Chan_IP2 = ChanList.length - 1;

ChanList.push(BuildChannel("IP (Floodgate Proidiom), 239.50.50.11:1234, via IP Proidiom", '<ChannelParams ChannelType="UDP" Encryption="Proidiom" AuthRequired="TRUE"> <UDPChannelParams Address="239.50.50.11" Port="1234"> </UDPChannelParams> </ChannelParams>'));

ChanList.push(BuildChannel("IP (Verimatrix Test), 239.255.224.24:1234", '<ChannelParams ChannelType="UDP"> <UDPChannelParams Address="239.255.224.24" Port="1234"> </UDPChannelParams> </ChannelParams>'));
Chan_IP3 = ChanList.length - 1;

ChanList.push(BuildChannel("CableCARD TimeWarner (Discovery), Tune by Freq/Prog", '<ChannelParams ChannelType="Cablecard" > <CablecardChannelParams PhysicalChannelIDType="Freq" Frequency="813000000" DemodMode="Auto" SourceID="4178" ProgramSelectionMode="PATProgram" ProgramID="1"> </CablecardChannelParams> </ChannelParams>'));
Chan_CableCard1 = ChanList.length -1;
ChanList.push(BuildChannel("CableCARD TimeWarner (History HD), Tune by Ring (source_id)", '<ChannelParams ChannelType="Cablecard" > <CablecardChannelParams SourceID="16098" /> </ChannelParams>'));
Chan_CableCard2 = ChanList.length -1;
ChanList.push(BuildChannel("CableCARD TimeWarner (NBC Sports), Tune by Ring (VC number)", '<ChannelParams ChannelType="Cablecard" > <CablecardChannelParams VirtualChannelNumber="31" /> </ChannelParams>'));
Chan_CableCard3 = ChanList.length -1;

/*
ChanList.push(BuildChannel("IP (Kassenna), 239.20.255.50:8000", '<ChannelParams ChannelType="UDP"> <UDPChannelParams Address="239.20.255.50" Port="8000"> </UDPChannelParams> </ChannelParams>'));
*/

/*
ChanList.push(BuildChannel("RTSP, 192.168.100.42:5541/60010000", '<ChannelParams ChannelType="RTSP"> <RTSPChannelParams URL="rtsp://192.168.100.42:5541/60010000" OpenParams="assetUid=3b9aca10&amp;transport=MP2T/AVP/UDP&amp;ServiceGroup=3&amp;smartcard-id=0050C25CC0A9&amp;device-id=0050C25CC0A9&amphome-id=1005&amp;purchase-id=179000040"> </RTSPChannelParams> </ChannelParams>'));
*/

ChanList.push(BuildChannel("RTSP (Kassenna), 172.20.255.50/Df1.mp2", '<ChannelParams ChannelType="RTSP"> <RTSPChannelParams URL="rtsp://172.20.255.50/Df1.mp2"> </RTSPChannelParams> </ChannelParams>'));
Chan_RTSP1 = ChanList.length - 1;

ChanList.push(BuildChannel("RTSP (Kassenna), 172.20.255.50/trailerreel", '<ChannelParams ChannelType="RTSP"> <RTSPChannelParams URL="rtsp://172.20.255.50/trailerreel"> </RTSPChannelParams> </ChannelParams>'));

ChanList.push(BuildChannel("RTSP (Infovalue), 192.168.255.11/Bikini.mp2", '<ChannelParams ChannelType="RTSP"> <RTSPChannelParams URL="rtsp://192.168.255.11/Bikini.mp2"> </RTSPChannelParams> </ChannelParams>'));

ChanList.push(BuildChannel("Input, Composite1", '<ChannelParams ChannelType="Input"> <InputChannelParams InputID="Composite1"> </InputChannelParams> </ChannelParams>'));
Chan_Input1 = ChanList.length - 1;

ChanList.push(BuildChannel("Input, VGA0", '<ChannelParams ChannelType="Input"> <InputChannelParams InputID="VGA/RGB0"> </InputChannelParams> </ChannelParams>'));

ChanList.push(BuildChannel("Playlist (Cassini)", '<ChannelParams ChannelType="File"> <FileChannelParams PlaylistId="0" /> </ChannelParams>'));
Chan_File1 = ChanList.length -1;
/*
ChanList.push(BuildChannel("Playlist - MPEG TS on the Hard Drive", '<ChannelParams ChannelType="File"> <FileChannelParams> <Playlist id="123" drive="Hard Drive" loop="0"> <MPEG name="Oahu_seamless_GOOD.trp" location="content"/> </Playlist> </FileChannelParams> </ChannelParams>'));
*/													
/*
ChanList.push(BuildChannel("Playlist - Audio File on the System Drive",	'<ChannelParams ChannelType="File"> <FileChannelParams> <Playlist id="123" drive="System Drive" loop="0"> <MPEG name="ding.wav" location=""/> </Playlist> </FileChannelParams> </ChannelParams>'));
*/

ChanList.push(BuildChannel("Shoutcast Internet Radio", '<ChannelParams ChannelType="InternetRadio"> <InternetRadioChannelParams URL="http://scfire-ntc-aa03.stream.aol.com:80/stream/1040"> </InternetRadioChannelParams> </ChannelParams>'));

// Update channel

var Chan_RFUpdate = 	'<ChannelParams ChannelType="Digital"> <DigitalChannelParams PhysicalChannelIDType="Cable" PhysicalChannelID="83" DemodMode="QAM256" ProgramSelectionMode="PATProgram" ProgramID="1"> </DigitalChannelParams> </ChannelParams>';

var Chan_IPUpdate = 	'<ChannelParams ChannelType="UDP"> <UDPChannelParams Address="239.255.224.66" Port="1234"> </UDPChannelParams> </ChannelParams>';

// Set the overall default channel
var DefChannel = Chan_File1;

var ChanList1 = new Array();
ChanList1.push(BuildChannel2("ABC", 26, 1));
ChanList1.push(BuildChannel2("NBC", 26, 2));
ChanList1.push(BuildChannel2("CBS", 26, 3));

ChanList1.push(BuildChannel2("FOX", 27, 1));
ChanList1.push(BuildChannel2("CW", 27, 2));
//ChanList1.push(BuildChannel2("SYFY", 27, 3));

ChanList1.push(BuildChannel2("TV Land HD", 28, 1));
ChanList1.push(BuildChannel2("Spike HD", 28, 2));
ChanList1.push(BuildChannel2("ESPN2 HD", 28, 3));

ChanList1.push(BuildChannel2("NFL HD", 29, 1));
ChanList1.push(BuildChannel2("ESPN2 HD", 29, 2));
ChanList1.push(BuildChannel2("CNN HD", 29, 3));

ChanList1.push(BuildChannel2("FOX News HD", 30, 1));
ChanList1.push(BuildChannel2("Discovery HD", 30, 2));
ChanList1.push(BuildChannel2("TNT HD", 30, 3));

ChanList1.push(BuildChannel2("A&E HD", 31, 1));
ChanList1.push(BuildChannel2("USA", 31, 2));
ChanList1.push(BuildChannel2("tbs HD", 31, 3));

ChanList1.push(BuildChannel2("History HD", 32, 1));
ChanList1.push(BuildChannel2("Comedy HD", 32, 2));
ChanList1.push(BuildChannel2("Cartoon HD", 32, 3));

ChanList1.push(BuildChannel2("abc family HD", 33, 1));
ChanList1.push(BuildChannel2("Nick HD", 33, 2));
ChanList1.push(BuildChannel2("PBS", 33, 3));

ChanList1.push(BuildChannel2("ESPNU", 34, 1));
ChanList1.push(BuildChannel2("ESPN HD", 34, 2));
ChanList1.push(BuildChannel2("ESPN Classic", 34, 3));

ChanList1.push(BuildChannel2("HLN HD", 35, 1));
ChanList1.push(BuildChannel2("CNBC HD", 35, 2));
ChanList1.push(BuildChannel2("Weather", 35, 3));

ChanList1.push(BuildChannel2("HGTV HD", 36, 1));
ChanList1.push(BuildChannel2("TLC HD", 36, 2));
ChanList1.push(BuildChannel2("Food HD", 36, 3));

ChanList1.push(BuildChannel2("E! HD", 37, 1));
ChanList1.push(BuildChannel2("Univision HD", 37, 2));
ChanList1.push(BuildChannel2("VH1", 37, 3));
ChanList1.push(BuildChannel2("MTV", 37, 4));

//Build a channel list object
function BuildChannel2(Label, phyChan, program) {
	var desc = '<ChannelParams ChannelType="Digital" Encryption="Proidiom" AuthRequired="TRUE" FastCCBuffering="FALSE">' +
			   '<DigitalChannelParams PhysicalChannelIDType="CableSTD" PhysicalChannelID="' + phyChan + '" DemodMode="QAM256" ' +
			   'ProgramSelectionMode="PATProgram" ProgramID="' + program + '"> </DigitalChannelParams> </ChannelParams>';
	var ChanObj = new Object();
	ChanObj.ChanDesc = desc;
	ChanObj.phyChan = phyChan;
	ChanObj.program = program;
	ChanObj.Label = Label + " " + phyChan + "." + program;
	return ChanObj;
}

ChanListRing = ChanList1;

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
