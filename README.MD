# Enseo SDK #

This repository holds SDK files and generic sample code for Enseo devices.

You can also [download](https://bitbucket.org/aceso/sdk-enseo/downloads) all archived SDKs.