#!/bin/sh

####################################################################
# Reference   : $Source: /cvsroot/calica/apps/stb/build_sfdi.sh,v $
# Revision    : $Revision: 1.13 $
# Date        : $Date: 2010-06-28 21:01:07 $
# Description : Script to build an initialization image for the system
#             : flash drive of the HD2000/HD4400/HD3000.  The HD2000 uses
#             : a JFFS2 image.  The HD4400/HD3000 uses an ext3 image.
####################################################################

usage() {
	echo "Usage: $0  <source directory> <output filename> <platform> [<build_trp>]"
	echo "           [--build-trp] [--preserve-owner]"
	echo
	echo " Note that the order of parameters is significant except for options"
    echo " starting with '--'.  These options must follow all non-optional or"
    echo " legacy parameters."
	echo
	echo " <source directory> is the directory containing the files and directories"
	echo "                    to include in the image"
	echo " <output filename>  is the name of the output jffs2 image."
    echo " <platform>         is either HD2000, HD4400 or HD3000."
	echo " [<build_trp>]      is either 'y' or 'n'. Select whether to build an eoc & trp file."
	echo "                    Default 'n'.  This is a legacy option and is deprecated."
	echo "                    --build-trp should be used instead."
	echo
	echo " [--build-trp]      Optionally build an eoc & trp file. Default is not to build eoc/trp."
	echo "                    Can be used with other optional options."
	echo " [--preserve-owner] Optionally preserve ownership of files within the generated sfdi image."
	echo "                    The default is to not preserve ownership. HD3000 and HD4400 only."
	exit 1
}

if [ $# -lt 2 ]; then
	echo "$0:  Error, missing arguments."
	usage
fi

source_dir=$1
outfile=$2

build_trp="n"
preserve_owner="n"

if [ $# -eq 2 ]; then
	platform="HD2000"
else
	platform=$3
fi

for opt in $* ; do
	case ${opt} in
		--build-trp)
			build_trp="y"
			;;
		--preserve-owner)
			preserve_owner="y"
			;;
		--*)
			echo "Unrecognised option ${opt}"
			usage
			;;
		*)
			;;
	esac
done

if [ $platform = "HD2000" ]; then

	# Size of JFFS2 partition in MBytes
	#	10.5 MByte in size (0x0a00000)
	#partition_size_MBytes=10	
	#	18.9 MByte in size (0x1200000)
	partition_size_MBytes=18

	# Calc total size of JFFS2 partition in bytes
	size_bytes=`expr $partition_size_MBytes \* 1024 \* 1024`
	echo "Total partition size= $size_bytes bytes"
		
	mkfs.jffs2 --pad=$size_bytes --eraseblock=128KiB -d ${source_dir} -o ${outfile}
	result=$?
	if [ $result != "0" ]; then
		echo "Error making jffs2 image, $result"
		exit 1
	fi

elif [ $platform = "HD3000" ]; then
	echo "build_sfdi.sh is no longer required for the HD3000."
	exit 0
elif [ $platform = "HD4400" ]; then
	error="y"
	# Size of ext3 partition in MBytes
	partition_size_MBytes=32
	size_KBytes=`expr $partition_size_MBytes \* 1024`
	echo "Total partition size= $size_KBytes Kbytes"
	dd if=/dev/zero of=${outfile} bs=1k count=$size_KBytes
	if [ $? != "0" ]; then
		echo "Error zeroing image"
	else
		echo "Creating filesystem using" `which mkfs.ext3`
		mkfs.ext3 -F -b 2048 ${outfile}
		if [ $? != "0" ]; then
			echo "Error making ext3 image"
		else
			# Disable time-dependent and num-mounts dependent checking of the filesystem.
			echo "Tuning filesystem using" `which tune2fs`
			tune2fs -i 0 -c 0 ${outfile}
			if [ $? != "0" ]; then
				echo "Error setting filesystem options"
			else
				mkdir .tmpMnt
				if [ $? != "0" ]; then
					echo "Error creating temporary mount directory"
				else
					sudo mount -o loop ${outfile} .tmpMnt
					if [ $? != "0" ]; then
						echo "Error mounting filesystem"
					else
						if [ $preserve_owner = "y" ]; then
							# Preserve file ownership
							cp --force --recursive --preserve=ownership,mode $source_dir/* .tmpMnt
						else
						    # Default: Using sudo changes ownership to root.
							sudo cp --force --recursive --no-preserve=ownership --preserve=mode $source_dir/* .tmpMnt
							# Make sure files are readable.
							sudo find .tmpMnt -type f ! -perm -444 -exec chmod a+r {} \;
						fi
						if [ $? != "0" ]; then
							echo "Error copying source files to file system image"
						else 
							error="n"
						fi
						sudo umount .tmpMnt
					fi
					rm -rf .tmpMnt
				fi
			fi				
		fi
	fi
	if [ $error = "y" ]; then
		rm --force ${outfile}
		exit 1
	fi
else
	echo "Invalid platform: $platform"
	exit 1
fi

if [ $# -ge 4 -a "$4" = "y" ] || [ $build_trp = "y" ] ; then

	h2d() {
		echo "obase=10;ibase=16; $1" | bc
	}

	h2quad() {
		bits00to07=`echo $1 | cut -c 7-8 | tr "[:lower:]" "[:upper:]"`
		bits08to15=`echo $1 | cut -c 5-6 | tr "[:lower:]" "[:upper:]"`
		bits16to23=`echo $1 | cut -c 3-4 | tr "[:lower:]" "[:upper:]"`
		bits24to31=`echo $1 | cut -c 1-2 | tr "[:lower:]" "[:upper:]"`
		echo `h2d ${bits24to31}`.`h2d ${bits16to23}`.`h2d ${bits08to15}`.`h2d ${bits00to07}`
	}

	group_id="0.0.0.0"
	uid="0.0.0.0"
	uid_hex="00000000"
	# Auto-dl disabled..?
	flags="0.0.0.0"

	eosysdrive=${source_dir}/.eosysdrive
	if [ -f ${eosysdrive} ]; then
	    # Pull version number from .eosysdrive file...
		uid_hex=`awk -Fuid= '/uid/ {S=substr($2,4);print substr(S,1,index(S,"\"")-1)}' ${eosysdrive}`
	else
	    # Compute a 'uid' string by taking md5sum and snarfing first 8 bytes of it.
	    uid_hex=`md5sum ${outfile} | cut -c 1-8`
	fi
	uid=`h2quad ${uid_hex}`

    # Create EOC file
	eoc_image=${outfile}.eoc
	eoconvert2ts --in=${outfile}+1792+0+${group_id}+0.0.0.0+255.255.255.255+${uid}+255.255.255.255+${flags} --verbose --preprocess --out=${eoc_image}

    # Create TRP file
	trp_image=${outfile}.trp
	eoconvert2ts --in=${eoc_image} --out=${trp_image} --bogus=3:1
fi



