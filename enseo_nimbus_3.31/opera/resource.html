<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Opera Devices Linux SDK 9.6: Resource Consumption</title>
    <link rel="stylesheet" type="text/css" href="gogisdk2.css"/>
    <link rel="author" href="http://www.opera.com" />
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="contents" href="index.html" />
    
<style type="text/css">
		table {	
			border: 1px #d79900 solid;
			border-collapse: collapse;
			border-spacing: 0px;
		}	
		td	{
			border: 1px #000000 solid;
		}				
	</style>
    
</head>

<body>

&copy; Opera Software ASA 2008 

<div id="top">
</div>
<div id="content">

<h1>Resource Consumption</h1>

<h2>RAM Cache</h2>
<p>
The RAM consumption is often of high importance on an embedded
device. This section explains the different RAM Caches which together
account for a lot of Opera's memory requirements.
</p>

<h3>Document Cache</h3>
<p>
The "Document Cache" holds the current page(s) and their document
structure, and as much as possible of recently visited pages. It is
allocated dynamically on the Opera process heap. The absolute minimum
stored in this cache is the full text of the main document and the
text of any CSS documents. The document structure is also stored in
this cache, and will contain elements for all tags (real and
implicit).
</p>

<p>
Text is stored in Unicode 16bits/char, with some overhead per word and
line, so this area normally requires 4-6 times the pages source size.
</p>

<p>
If the user has several windows open (even if hidden), the current
pages of all windows will be in this cache.
</p>

<p>
For devices with limited RAM, we try to keep this area at a
minimum. When we set the size to 200Kb, it will grow as needed, but
space will be reclaimed immediately when the page is not current
anymore.
</p>

<p>
Note that not all memory allocated for a document is counted, for
example, memory used by Java/ECMAscript is not included in the
document's RAM cache usage.
</p>

<table>
  <tr>
    <th>Section</th>
    <th>Entry</th>
    <th>Default</th>
    <th>Description</th>
  </tr>
  <tr>
    <td>[Cache]</td>
    <td>Document</td>
    <td>400</td>
    <td>Size of document RAM cache</td>
  </tr>
</table>

<h3>Image cache</h3>
<p>
Opera holds decoded (uncompressed) images in the image cache. The size
of the image cache is configurable. The image cache size limit is not
strictly adhered to in the Opera SDK for Devices, it may be exceeded
if necessary to show all images on the page.
</p>

<p>
For animated images, the image cache must also hold the various layers
or frames.
</p>

<p>
All visible images must be entirely in the image cache. Once they are
no longer visible they may be thrown out if new images won't fit.
</p>

<p>
When Opera renders scaled images (typical scenario when using
fit-to-screen), the complete rendered image must be in the image
cache, plus rendered, scaled segments of the image that is visible on
screen.
</p>

<table>
  <tr>
    <th>Section</th>
    <th>Entry</th>
    <th>Default</th>
    <th>Description</th>
  </tr>
  <tr>
    <td>[Cache]</td>
    <td>Figure</td>
    <td>4000</td>
    <td>Size of image RAM cache</td>
  </tr>
</table>

<h3>Java/ECMAscript storage</h3>
<p>
JavaScript objects are stored on a garbage collected heap.  The number
of live objects in the heap is the sum of the numbers of live objects
for all documents that are kept in RAM (current documents and cached
documents).
</p>

<p>
When the JavaScript heap is full, the garbage collector is run to
reclaim objects that are no longer reachable. Following the collection
the heap is resized to accommodate 1.5 times as many objects as were
found to be live by the garbage collector.
</p>

<p>
When a new document is loaded into Opera, the size of the JavaScript
heap is checked against a fixed heap limit parameter. If the heap size
exceeds the set limit, then documents are evicted from the memory
cache and the JavaScript garbage collector is run repeatedly until the
heap size no longer exceeds the limit or no more documents can be
evicted.
</p>

<table>
  <tr>
    <th>Section</th>
    <th>Entry</th>
    <th>Default</th>
    <th>Description</th>
  </tr>
  <tr>
    <td>[Cache]</td>
    <td>ECMAScript</td>
    <td>400</td>
    <td>The size of heap space allocated for scripts</td>
  </tr>
</table>

<h3>Summary</h3>
<p>
Note that not all memory allocated for a document is counted, for
example, memory used by Java/ECMAscript is not included in the
document's RAM cache usage.
</p>

<table>
  <tr>
    <th>Section</th>
    <th>Entry</th>
    <th>Default</th>
    <th>Description</th>
  </tr>
  <tr>
    <td>[Cache]</td>
    <td>Document</td>
    <td>400</td>
    <td>Size of document RAM cache</td>
  </tr>
  <tr>
    <td>[Cache]</td>
    <td>Figure</td>
    <td>4000</td>
    <td>Size of image RAM cache</td>
  </tr>
  <tr>
    <td>[Cache]</td>
    <td>ECMAscript</td>
    <td>400</td>
    <td>Size of ECMAscript RAM cache</td>
  </tr>
</table>

<h2>Summary</h2>

<h3>Memory implications of the configuration settings</h3>

<p>
Opera will allocate the Document Cache, the Image Cache and JavaScript
storage from heap on start-up and it will fill up as the user
browse. When these areas are full, they will normally stay close to
full, as memory is not re-claimed below the configured limits. When
the user browses pages that are larger than what fits into the
document cache and JavaScript storage, the area will grow further as
needed. This is also true if the user opens several documents in
several windows, as documents in all open windows are considered
&quot;current&quot; and therefore locked in the document cache.
</p>

<p>
The cache sizes and disk cache usage can be controlled by the
different preferences in the [Cache] and [Disk Cache] sections of <a
href="opera_ini.html">opera.ini</a>. Tuning these can lead to
significant reductions in memory consumption, but will always be a
trade-off with speed.
</p>

<table>
  <tr>
    <th>Storage element</th>
    <th>Typical RAM impact</th>
    <th>Heavy usage</th>
    <th>Comments</th>
  </tr>
  <tr>
    <td>Document cache</td>
    <td>400</td>
    <td>5000</td>
    <td>Not strict, may grow until system halts </td>
  </tr>
  <tr>
    <td>Image cache</td>
    <td>4000</td>
    <td>4000</td>
    <td>Strict</td>
  </tr>
  <tr>
    <td>ECMAscript Heap </td>
    <td>400</td>
    <td>400</td>
    <td>May grow but normally kept in this range </td>
  </tr>
  <tr>
    <td>Various allocations</td>
    <td>500</td>
    <td>1000</td>
    <td>Allocated by Opera at start-up and while browsing</td>
  </tr>
  <tr>
    <td>Total</td>
    <td>5300</td>
    <td>10400</td>
    <td>&nbsp;</td>
  </tr>
</table>

<h3>Algorithms</h3>
<table>
  <tr>
    <th>Cache</th>
    <th>Strict</th>
    <th>Configurable</th>
    <th>Memory</th>
    <th>Cache policy</th>
  </tr>
  <tr>
    <td>Document cache</td>
    <td>No</td>
    <td>Run-time</td>
    <td>RAM</td>
    <td>LRU</td>
  </tr>
  <tr>
    <td>Image cache</td>
    <td>Yes</td>
    <td>Run-time</td>
    <td>RAM</td>
    <td>LRU</td>
  </tr>
  <tr>
    <td>ECMAscript garbage collector</td>
    <td>No</td>
    <td>Somewhat</td>
    <td>RAM</td>
    <td>GC</td>
  </tr>
</table>

<h2>Dictionary</h2>
<dl>

<dt>LRU</dt>
<dd>Least recently used. The object in a cache that will be thrown out first.</dd>
<dt>GC</dt>
<dd>Garbage collector.</dd>
<dt>Run-time</dt>
<dd>Value can be changed any time.</dd>
<dt>Active document</dt>
<dd>The document that the user is currently interacting with.</dd>

</dl>

<p>
&copy; Opera Software ASA 2008
</p>

</div>
</body> </html>
